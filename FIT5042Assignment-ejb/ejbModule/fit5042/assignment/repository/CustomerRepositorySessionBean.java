package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;


/**
 * @author Administrator
 *
 */
@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext (unitName = "FIT5042Assignment-ejb")
	private EntityManager entityManager;
	
	@Override
	public void addCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
        List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
    }
	
	@Override
	public Customer searchCustomerById(int id) throws Exception {
		// TODO Auto-generated method stub
        Customer customer = entityManager.find(Customer.class, id);
        customer.getClass();
        return customer;
    }

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}
	
	@Override
	public List<ContactPerson> getAllContactPeople() throws Exception {
		// TODO Auto-generated method stub
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		// TODO Auto-generated method stub
    	Customer customer = this.searchCustomerById(customerId);

        if (customer != null) {
            entityManager.remove(customer);
        }
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
        try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
	}

	@Override
	public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) throws Exception {
		// TODO Auto-generated method stub
        contactPerson = entityManager.find(ContactPerson.class, contactPerson.getConactPersonId());
        contactPerson.getCustomers().size();
        entityManager.refresh(contactPerson);

        return contactPerson.getCustomers();
	}

}