/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {

    private int customerId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String dob;
    private String industry;
    
    private Address address;
    private ContactPerson contactPerson;
    
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    
    private Set<fit5042.assignment.repository.entities.Customer> customers;
    
    public Set<fit5042.assignment.repository.entities.Customer> getCustomers() {
		return customers;
	}



	public void setCustomers(Set<fit5042.assignment.repository.entities.Customer> customers) {
		this.customers = customers;
	}



	public String getPhoneNumber1() {
		return phoneNumber1;
	}



	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getDob1() {
		return dob1;
	}



	public void setDob1(String dob1) {
		this.dob1 = dob1;
	}



	public String getExperience() {
		return experience;
	}



	public void setExperience(String experience) {
		this.experience = experience;
	}



	private int conactPersonId;
    private String name;
    private String phoneNumber1;
    private String gender;
    private String dob1;
    private String experience;

    
    public Customer(int customerId, String firstName, String lastName, String phoneNumber, String email, String dob,
			String industry, Address address, ContactPerson contactPerson) {
		super();
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.dob = dob;
		this.industry = industry;
		this.address = address;
		this.contactPerson = contactPerson;
	}



	public int getCustomerId() {
		return customerId;
	}



	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}



	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getDob() {
		return dob;
	}



	public void setDob(String dob) {
		this.dob = dob;
	}



	public String getIndustry() {
		return industry;
	}



	public void setIndustry(String industry) {
		this.industry = industry;
	}



	public Address getAddress() {
		return address;
	}



	public void setAddress(Address address) {
		this.address = address;
	}



	public ContactPerson getContactPerson() {
		return contactPerson;
	}



	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}



	public String getStreetNumber() {
		return streetNumber;
	}



	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}



	public String getStreetAddress() {
		return streetAddress;
	}



	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}



	public String getSuburb() {
		return suburb;
	}



	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}



	public String getPostcode() {
		return postcode;
	}



	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public int getConactPersonId() {
		return conactPersonId;
	}



	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
    
    
    
}
