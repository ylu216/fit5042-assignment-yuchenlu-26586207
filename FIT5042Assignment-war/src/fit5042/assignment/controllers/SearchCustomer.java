package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;

/*
 * To change this license header, choose License Headers in Project Customer.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
    private boolean showForm = true;

    private Customer customer;

    CustomerApplication app;

    private int searchByInt;


    public CustomerApplication getApp() {
        return app;
    }

    public void setApp(CustomerApplication app) {
        this.app = app;
    }


    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }




    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateCustomerList();

    }

    /**
     * Normally each page should have a backing bean but you can actually do it
     * any how you want.
     *
     * @param customer Id
     */
    public void searchCustomerById(int customerId) {
        try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchCustomerById(customerId);
        } catch (Exception ex) {

        }
        showForm = true;

    }

    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
