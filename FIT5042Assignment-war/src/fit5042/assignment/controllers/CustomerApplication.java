package fit5042.assignment.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.inject.Named;
import fit5042.assignment.repository.entities.Customer;
import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {

    //dependency injection of managed bean here so that we can use its methods
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private ArrayList<Customer> customers;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

    // Add some property data from db to app 
    public CustomerApplication() throws Exception {
        customers = new ArrayList<>();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private void setCustomers(ArrayList<Customer> newCustomers) {
        this.customers = newCustomers;
    }

    //when loading, and after adding or deleting, the property list needs to be refreshed
    //this method is for that purpose
    public void updateCustomerList() {
        if (customers != null && customers.size() > 0)
        {
            
        }
        else
        {
            customers.clear();

            for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
            {
                customers.add(customer);
            }

            setCustomers(customers);
        }
    }

    public void searchCustomerById(int customerId) {
        customers.clear();

        customers.add(customerManagedBean.searchCustomerById(customerId));
    }


    
    public void searchAll()
    {
    	customers.clear();
        
        for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
}
