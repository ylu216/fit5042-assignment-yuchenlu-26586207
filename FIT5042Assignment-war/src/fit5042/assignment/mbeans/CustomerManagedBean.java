package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.ContactPerson;


@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
    @EJB
    CustomerRepository customerRepository;
    
    public CustomerManagedBean() {
    }
	
    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addCustomer(Customer localCustomer) {
        try {
            customerRepository.addCustomer(localCustomer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Search a customer by Id
     */
    public Customer searchCustomerById(int id) {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) {
        try {
            return customerRepository.searchCustomerByContactPerson(contactPerson);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public List<ContactPerson> getAllContactPeople() throws Exception {
        try {
            return customerRepository.getAllContactPeople();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public void removeCustomer(int customerId) {
        try {
            customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editCustomer(Customer customer) {
        try {
            String s = customer.getAddress().getStreetNumber();
            Address address = customer.getAddress();
            address.setStreetNumber(s);
            customer.setAddress(address);

            customerRepository.editCustomer(customer);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    public void addCustomer(fit5042.assignment.controllers.Customer localCustomer) {

        Customer customer = convertCustomerToEntity(localCustomer);

        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Customer convertCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer) {
        Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setAddress(address);
        customer.setFirstName(localCustomer.getFirstName());
        customer.setLastName(localCustomer.getLastName());
        customer.setPhoneNumber(localCustomer.getPhoneNumber());   
        customer.setEmail(localCustomer.getEmail());
        customer.setDob(localCustomer.getDob());
        customer.setIndustry(localCustomer.getIndustry());
        
        
        int conactPersonId = localCustomer.getConactPersonId();
        String name = localCustomer.getName();
        String phoneNumber1 = localCustomer.getPhoneNumber1();
        String gender = localCustomer.getGender();
        String dob1 = localCustomer.getDob1();
        String experience = localCustomer.getExperience();
        
        
        ContactPerson contactPerson = new fit5042.assignment.repository.entities.ContactPerson(conactPersonId, name, phoneNumber1, gender, dob1, experience, null);
        if (contactPerson.getConactPersonId() == 0) {
            contactPerson = null;
        }
        
        customer.setContactPerson(contactPerson);
        
        return customer;
    }
}
