package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;

/**
 * 2020/11/12
 * @author Yuchen Lu
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc")})
public class Customer implements Serializable {

    public static final String GET_ALL_QUERY_NAME = "Customer.getAll";

    private int customerId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String dob;
    private String industry;
    private Address address;
    private ContactPerson contactPerson;

    public Customer() {
    }
    
    public Customer(int customerId, String firstName, String lastName, String phoneNumber, String email, String dob,
			String industry, Address address, ContactPerson contactPerson) {
		super();
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.dob = dob;
		this.industry = industry;
		this.address = address;
		this.contactPerson = contactPerson;
	}
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    
    //insert annotation here to make address as embedded to customer entity and stored as part of customer
    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    //enforce the relationship between a customer and his contact person using annotation(s)
    @ManyToOne
    public ContactPerson getContactPerson() {
        return contactPerson;
    }
    

    public void setContactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@NotEmpty(message = "Phone number should not be empty")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Email(message = "Email is not valid, try again ")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", phoneNumber=" + phoneNumber + ", email=" + email + ", dob=" + dob + ", industry=" + industry
				+ ", address=" + address + ", contactPerson=" + contactPerson + "]";
	}

}
