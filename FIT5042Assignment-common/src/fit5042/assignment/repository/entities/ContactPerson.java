package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import fit5042.assignment.repository.entities.ContactPerson;

/**
 *
 * @author Yuchen Lu
 */
@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({
    @NamedQuery(name = ContactPerson.GET_ALL_QUERY_NAME, query = "SELECT c FROM ContactPerson c")})
public class ContactPerson implements Serializable {

    public static final String GET_ALL_QUERY_NAME = "ContactPerson.getAll";

    private int conactPersonId;
    private String name;
    private String phoneNumber;
    private String gender;
    private String dob;
    private String experience;
    private Set<Customer> customers;

    public ContactPerson() {
    }

    public ContactPerson(int conactPersonId, String name, String phoneNumber, String gender, String dob, String experience,
			Set<Customer> customers) {
		super();
		this.conactPersonId = conactPersonId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.gender = gender;
		this.dob = dob;
		this.experience = experience;
		this.customers = new HashSet<>();
	}



	@Id
    @GeneratedValue
    @Column(name = "contact_person_id")
    public int getConactPersonId() {
        return conactPersonId;
    }

    public void setConactPersonId(int conactPersonId) {
        this.conactPersonId = conactPersonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "phone_number")
	@NotEmpty(message = "Phone number should not be empty")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	//enforce the relationship between a property and its contact person using annotation(s). Each property has one and only one contact person. Each contact person might be responsible for zero to many properties
    @OneToMany(mappedBy = "contactPerson")
    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.conactPersonId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContactPerson other = (ContactPerson) obj;
        if (this.conactPersonId != other.conactPersonId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.conactPersonId + " - " + name + " - " + phoneNumber + " - " + gender + " - " + dob + " - " + experience;
    }
}
